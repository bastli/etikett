#!/bin/sh

exec amqp-consume --server="${RABBIT_SERVER}:${RABBIT_PORT}" --username="${RABBIT_USERNAME}" "--password=${RABBIT_PASSWORD}" --vhost="/" --queue="labelprint_queue" ./etikett.sh
