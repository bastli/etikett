etikett
=======

A label printing service.
Listens on a UDP port and sends a 44x27mm label to a printer.

Dependencies
------------

* [gle](http://glx.sourceforge.net/)
* [gnu barcode](https://www.gnu.org/software/barcode/)
* [socat](http://www.dest-unreach.org/socat/)
* m4
* sh
