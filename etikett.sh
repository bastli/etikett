#!/bin/sh

read part
read no

WIDTH=44
HEIGHT=27

BARCODE="$(mktemp --suffix=.eps)"

barcode -u mm -g "${WIDTH}x${HEIGHT}" -e "code128" -E -b "$no" -n > "${BARCODE}" &
m4 -DBARCODE="$BARCODE" -DNAME="$part" -DID="$no" -DWIDTH="${WIDTH}" -DHEIGHT="${HEIGHT}" etikett.gle.m4 \
	| gle -noctrl-d -d ps -output "etikett.ps" etikett.gle \
	| zathura -

rm "${BARCODE}"
